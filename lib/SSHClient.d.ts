import { ConnectConfig } from 'ssh2';
export declare class SSHClient {
    private conn;
    private cmdStdout;
    connect(hostinfo: ConnectConfig): Promise<number>;
    exec(cmd: string): Promise<string>;
    put(localPath: string, remotePath: string): Promise<number>;
    end(): Promise<number>;
}
