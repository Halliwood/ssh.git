# ssh
这是对 https://github.com/mscdex/ssh2 的包装。
以便更简单地远程上传文件。

# 安装教程

`npm i @taiyosen/ssh`

# 使用说明

```Typescript
const cfg = {
    cdnDir: '/var/www/html/dev/cdn_dir',
    hostinfo: {
      host: '127.0.0.1',
      port: 22,
      username: 'root',
      password: 'the_password'
    },
    srcResfile: 'D:/tmp/the_src_files',
    dstResfile: 'D:/tmp/the_src_files.tgz'
};

const ssh = new SSHClient();
await ssh.connect(cfg.hostinfo);
await ssh.exec(`rm ${cfg.dstResfile}`);
await ssh.put(cfg.srcResfile, cfg.dstResfile);

const osInfo = await ssh.exec('uname -a');
let md5cmd = 'md5sum';
if(osInfo.startsWith('Darwin')) {
    md5cmd = 'md5';
}

// 比较文件md5
const remoteMd5stdout = await ssh.exec(`${md5cmd} ${cfg.dstResfile}`);
const remoteMd5 = remoteMd5stdout.match(/[0-9a-fA-F]{32}/)![0].toLowerCase();
const localMd5 = md5(fs.readFileSync(cfg.srcResfile));
    
if(remoteMd5 != localMd5) {
    console.error('errer: md5 is not equal!')
    process.exit(1);
}
console.log('md5 is equal...')

await ssh.exec(`mkdir -p ${cfg.cdnDir}`);
await ssh.exec(`tar -xzf ${cfg.dstResfile} -C ${cfg.cdnDir}`);
await ssh.exec(`chmod -R o+rx ${cfg.cdnDir}`);
```

# 远程连接 

```Typescript
connect(hostinfo: ConnectConfig): Promise<number>
```

# 执行命令

```Typescript
exec(cmd: string): Promise<string>;
```

# 上传文件

```Typescript
put(localPath: string, remotePath: string): Promise<number>;
```

# 终止连接

```Typescript
end(): Promise<number>;
```